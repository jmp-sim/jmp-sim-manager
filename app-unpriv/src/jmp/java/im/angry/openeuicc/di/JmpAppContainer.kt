package im.angry.openeuicc.di

import android.content.Context

class JmpAppContainer(context: Context) : UnprivilegedAppContainer(context) {
    override val uiComponentFactory by lazy {
        JmpUiComponentFactory()
    }

    override val customizableTextProvider by lazy {
        JmpCustomizableTextProvider(context)
    }
}
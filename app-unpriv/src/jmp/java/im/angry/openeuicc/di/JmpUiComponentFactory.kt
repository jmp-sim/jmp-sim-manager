package im.angry.openeuicc.di

import androidx.fragment.app.Fragment
import im.angry.openeuicc.ui.JmpNoEuiccPlaceholderFragment

class JmpUiComponentFactory : UnprivilegedUiComponentFactory() {
    override fun createNoEuiccPlaceholderFragment(): Fragment =
        JmpNoEuiccPlaceholderFragment()
}
package im.angry.openeuicc

import im.angry.openeuicc.di.JmpAppContainer

class JmpSimManagerApplication : UnprivilegedOpenEuiccApplication() {
    override val appContainer by lazy {
        JmpAppContainer(this)
    }
}
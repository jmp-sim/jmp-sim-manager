JMP SIM Manager
---

This is a soft fork and branded version of [OpenEUICC](https://gitea.angry.im/PeterCxy/OpenEUICC) for the purpose of the [JMP eSIM Adapter](https://jmp.chat/esim-adapter).

The eSIM Adapter is a removable eSIM chip that enables the use of downloaded eSIM profiles on any device with a modem. New profiles can be downloaded with the JMP SIM Manager on an Android device, or through a PC/SC smart card reader plugged into the Android device. [lpac](https://github.com/estkme-group/lpac) is available for use on a Linux/Windows/macOS PC.

Releases can be found in the [release page](https://gitea.angry.im/jmp-sim/jmp-sim-manager/releases).

Copyright
===

Everything except `libs/lpac-jni` and `art/`:

```
Copyright 2022-2024 OpenEUICC contributors

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
```

`libs/lpac-jni`:

```
Copyright (C) 2022-2024 OpenEUICC contributiors

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation, version 2.1.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
```

`art/`: Courtesy of [Aikoyori](https://github.com/Aikoyori), CC NC-SA 4.0.
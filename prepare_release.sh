#!/usr/bin/env bash

set -e

new_version=$(($(git rev-list --first-parent --count HEAD) + 1))
echo "New version code will be: ${new_version}"
echo "${new_version}" > ./LAST_RELEASE_VERCODE

changelog_path="./fastlane/metadata/android/en-US/changelogs"
mkdir -p ${changelog_path}
changelog_file="${changelog_path}/${new_version}.txt"
echo "Creating new changelog: ${changelog_file}"
touch "${changelog_file}"
echo "Please populate the changelog above, then create ONE commit, and tag the release."